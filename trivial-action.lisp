(in-package :trivial-action)

(defclass action ()
  ((name :initarg :name
	 :reader action-name
	 :type keyword)
   (docu :initarg :docu
	 :reader action-documentation
	 :initform nil
	 :type (or string nil))
   (code :initarg :code
	 :reader code
	 :type string)
   (fn :initarg :fn
       :reader fn
       :initform nil
       ;;:type (or null function)
       ))
  (:metaclass closer-mop:funcallable-standard-class))

(defmacro defaction (name &body body)
  "*name* - A keyword identifying this action (e.g. :turn-light-off)  
   *body* - The action itself. The first expression in *body* might be a string, which then is taken to be the documentation."

  (let* ((docu (if (stringp (car body)) (car body) ""))
         (fn `(lambda (&rest args) (declare (ignorable args)) ,@(if (stringp (car body)) (cdr body) body))))
    `(make-instance 'action :name ,name :docu ,docu :code ,(format nil "#'~a" fn) :fn ,fn)))

(defmethod initialize-instance :after ((obj action) &key)
  (unless (and (slot-boundp obj 'fn) (fn obj))
    (setf (slot-value obj 'fn) (eval (read-from-string (code obj)))))
  (closer-mop:set-funcallable-instance-function obj (fn obj)))

(defun action-from-string (name docu code-string)
    (make-instance 'action :name name :docu docu :code code-string))

