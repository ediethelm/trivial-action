(uiop:define-package #:trivial-action
  (:documentation "")
  (:use #:common-lisp)
  (:export #:action
	   #:action-name
           #:action-documentation
           #:defaction
           #:action-from-string))

