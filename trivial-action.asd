;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-


(defsystem :trivial-action
  :name "trivial-action"
  :description ""
  :version "0.1.1"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :in-order-to ((test-op (test-op :trivial-action/test)))
  :depends-on (:closer-mop
	       :log4cl)
  :components ((:file "package")
	       (:file "trivial-action")))

(defsystem :trivial-action/test
  :name "trivial-action/test"
  :description "Unit Tests for the trivial-action project."
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-action
	       :fiveam)
  :perform (test-op (o s) (uiop:symbol-call :fiveam  '#:run! :trivial-action-tests))
  :components ((:file "test-trivial-action")))

